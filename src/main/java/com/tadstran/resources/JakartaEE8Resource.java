package com.tadstran.resources;

import com.google.gson.JsonObject;
import com.tadstran.dao.VehicleDAO;
import com.tadstran.model.Vehicle;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author
 */
@Path("vehicle")
public class JakartaEE8Resource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {

        VehicleDAO vehicleDAO = new VehicleDAO();
        Vehicle vehicle = vehicleDAO.get(1);
        System.out.println(vehicle);
        JsonObject jsonString = new JsonObject();

        jsonString.addProperty("model", vehicle.getModel());
        return jsonString.toString();
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tadstran.facade;

import com.tadstran.dao.LoginDAO;
import com.tadstran.model.User;

/**
 *
 * @author daniel
 */
public class LoginFacade {

    public static User fazerLogin(String email, String password) {
        LoginDAO loginDAO = new LoginDAO();
        return loginDAO.selectUsuario(email, password);
    }
}

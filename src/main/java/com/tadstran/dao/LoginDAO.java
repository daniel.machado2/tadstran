/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tadstran.dao;

import com.tadstran.model.User;
import com.tadstran.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author daniel
 */
public class LoginDAO {

    public User selectUsuario(String email, String password) {
        User user = new User();
        String HQL = "from users u where email = :email and senha = :password";
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = (Query) session.createQuery(HQL)
                    .setParameter("email", email)
                    .setParameter("password", password);
            user = (User) query.uniqueResult();
        } catch (Exception e) {
        }
        return user;
    }
}

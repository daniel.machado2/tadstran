package com.tadstran.dao;

import java.util.List;

import com.tadstran.model.Address;
import com.tadstran.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import org.hibernate.criterion.Restrictions;

/**
 *
 * @author daniel
 */
public class AddressDAO {

    // post
    public boolean insert(Address address) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(address);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    // get
    public Address get(int id) {
        Address address = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Criteria criteria = session.createCriteria(Address.class);
            criteria.add(Restrictions.eq("id", id));
            
            address = (Address) criteria.uniqueResult();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return address;
    }

    // all
    public List<Address> all() {
        List<Address> addresses = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            
            Criteria criteria = session.createCriteria(Address.class);
            addresses = criteria.list();
            
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return addresses;
    }

    // update
    public boolean update(Address address) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(address);
            session.getTransaction().commit();
            session.clear();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean delete(Address address) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            session.delete(address);
            tx.commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}

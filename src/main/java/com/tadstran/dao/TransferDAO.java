package com.tadstran.dao;

import java.util.List;

import com.tadstran.model.Transfer;
import com.tadstran.util.HibernateUtil;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author daniel
 */
public class TransferDAO {
    // post
    public boolean insert(Transfer transfer) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(transfer);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    // get
    public Transfer get(int id) {
        Transfer transfer = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Criteria criteria = session.createCriteria(Transfer.class);
            criteria.add(Restrictions.eq("id", id));

            transfer = (Transfer) criteria.uniqueResult();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return transfer;
    }

    // all
    public List<Transfer> all() {
        List<Transfer> transfers = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Criteria criteria = session.createCriteria(Transfer.class);
            transfers = criteria.list();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return transfers;
    }

    // update
    public boolean update(Transfer transfer) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(transfer);
            session.getTransaction().commit();
            session.clear();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean delete(Transfer transfer) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            session.delete(transfer);
            tx.commit();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}

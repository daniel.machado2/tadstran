package com.tadstran.dao;

import java.util.List;

import com.tadstran.model.Vehicle;
import com.tadstran.util.HibernateUtil;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author daniel
 */
public class VehicleDAO {
    // post
    public boolean insert(Vehicle vehicle) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(vehicle);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    // get
    public Vehicle get(int id) {
        Vehicle vehicle = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Criteria criteria = session.createCriteria(Vehicle.class);
            criteria.add(Restrictions.eq("id", id));

            vehicle = (Vehicle) criteria.uniqueResult();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return vehicle;
    }

    // all
    public List<Vehicle> all() {
        List<Vehicle> vehicles = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Criteria criteria = session.createCriteria(Vehicle.class);
            vehicles = criteria.list();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return vehicles;
    }

    // update
    public boolean update(Vehicle vehicle) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(vehicle);
            session.getTransaction().commit();
            session.clear();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean delete(Vehicle vehicle) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            session.delete(vehicle);
            tx.commit();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package com.tadstran.model;

/**
 *
 * @author daniel
 */
public enum Fuel {
    alcohol, gasoline, flex
}

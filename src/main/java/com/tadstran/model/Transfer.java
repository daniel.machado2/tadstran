/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tadstran.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author daniel
 */
@Entity
@Table(name = "transfers")
public class Transfer implements Serializable {

    private Long id;
    private Collection<User> originUsers;
    private Collection<User> destinationUsers;
    private Collection<Vehicle> vehicles;
    private Date transferDate;
    private Float amount;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transfer_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToMany(targetEntity = User.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinTable(name = "transfers",
            joinColumns = {
                @JoinColumn(name = "transfer_id")},
            inverseJoinColumns = {
                @JoinColumn(name = "origin_user_id")})
    public Collection<User> getOriginUsers() {
        return originUsers;
    }

    public void setOriginUsers(Collection<User> originUsers) {
        this.originUsers = originUsers;
    }

    @ManyToMany(targetEntity = User.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinTable(name = "transfers",
            joinColumns = {
                @JoinColumn(name = "transfer_id")},
            inverseJoinColumns = {
                @JoinColumn(name = "destination_user_id")})
    public Collection<User> getDestinationUsers() {
        return destinationUsers;
    }

    public void setDestinationUsers(Collection<User> destinationUsers) {
        this.destinationUsers = destinationUsers;
    }

    @ManyToMany(targetEntity = Vehicle.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinTable(name = "transfers",
            joinColumns = {
                @JoinColumn(name = "transfer_id")},
            inverseJoinColumns = {
                @JoinColumn(name = "vehicle_id")})
    public Collection<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(Collection<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "transfer_date")
    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    @Column(name = "amount")
    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

}

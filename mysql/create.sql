-- Database: tadstran
-- drop database tadstran;
-- create database tadstran;
CREATE TYPE tipoenum AS ENUM('f','p');
CREATE TYPE fuelenum as ENUM('ALCOHOL', 'GASOLINE', 'FLEX');

create table if not exists users(
	user_id int,
	email VARCHAR(30),
	senha varchar,
	cpf varchar(14),
	rg varchar(12),
	nome varchar(50),
	tipo tipoenum,
	CONSTRAINT pk_user PRIMARY KEY (user_id)
);

create table if not exists vehicles(
	vehicle_id int,
	renavam varchar(30),
    chassi VARCHAR(30),
    brand VARCHAR(30),
    model VARCHAR(30),
    year_of_manufacture date,
    model_year date,
    fuel fuelenum,
    color VARCHAR(30),
    licensePlate VARCHAR(30),
    user_id int,
	CONSTRAINT pk_vehicle PRIMARY KEY (vehicle_id),
	CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users(user_id) 
);

CREATE TABLE if not exists transfer(
	transfer_id int,
	origin_user_id int,
	destination_user int,
	vehicle_id int,
	transfer_date date,
	amount float,
	CONSTRAINT pk_transfer PRIMARY KEY (transfer_id),
	CONSTRAINT fk_origin_user_id FOREIGN KEY (origin_user_id) REFERENCES users(user_id), 
	CONSTRAINT fk_destination_user FOREIGN KEY (destination_user) REFERENCES users(user_id),
	CONSTRAINT fk_vehicle_id FOREIGN KEY (vehicle_id) REFERENCES vehicles(vehicle_id)
);

create table if not exists address(
	address_id int primary key,
	street VARCHAR(30),
	user_id int,
	CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users(user_id) 
);
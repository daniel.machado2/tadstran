-- Database: tadstran
drop database tadstran;
create database tadstran;
use tadstran;

create table if not exists addresses(
	address_id int auto_increment,
	street varchar(50) not null, 
	number int not null,
	complement varchar(50), 
	neighborhood varchar(50), 
	zip_code varchar(11),
	city varchar(30), 
	state varchar(30),
    CONSTRAINT pk_address PRIMARY KEY (address_id)
);

create table if not exists users(
	user_id integer auto_increment not null,
	email VARCHAR(30),
	senha varchar(100),
	cpf varchar(14),
	rg varchar(12),
	nome varchar(50),
	tipo enum('f','p'),
	CONSTRAINT pk_user PRIMARY KEY (user_id)
);

alter table users add address_id integer not null references addresses(address_id);

create table if not exists vehicles(
	vehicle_id int auto_increment,
	renavam varchar(30),
    chassi VARCHAR(30),
    brand VARCHAR(30),
    model VARCHAR(30),
    year_of_manufacture date,
    model_year date,
    fuel  enum('alcohol','gasoline','flex'),
    color VARCHAR(30),
    license_plate VARCHAR(30),
    owner_id int,
	CONSTRAINT pk_vehicle PRIMARY KEY (vehicle_id),
	CONSTRAINT fk_user FOREIGN KEY (owner_id) REFERENCES users(user_id) 
);

CREATE TABLE if not exists transfers(
	transfer_id int auto_increment,
	origin_user_id int,
	destination_user_id int,
	vehicle_id int,
	transfer_date date,
	amount float,
	CONSTRAINT pk_transfer PRIMARY KEY (transfer_id),
	CONSTRAINT fk_origin_user_id FOREIGN KEY (origin_user_id) REFERENCES users(user_id), 
	CONSTRAINT fk_destination_user FOREIGN KEY (destination_user_id) REFERENCES users(user_id),
	CONSTRAINT fk_vehicle_id FOREIGN KEY (vehicle_id) REFERENCES vehicles(vehicle_id)
);

insert into addresses(street, number, complement,neighborhood,zip_code,city,state) values ("Avenida Principal", 3, "", "Centro", "80.010-130", "Mundial", "MD");
insert into users(cpf, nome, rg, email, address_id, tipo) values ('088.824.129-11', 'Rafael', '32.639.581-7', 'teste@gmail.com', 1, 'p');
insert into users(cpf, nome, rg, email, address_id, tipo) values ('088.824.149-11', 'Gabriel', '32.649.581-7', 'teste2@gmail.com', 2, 'f');
insert into vehicles(renavam,chassi,brand,model,year_of_manufacture, model_year,fuel,color,license_plate,owner_id) values 
('21197960068', 'XASKJASD', 'Acura', 'Integra GS 1.8', '1999-03-04', '1999-03-03', 'gasoline', 'branco', 'JTE-3928', 1);
insert into transfers(origin_user_id,destination_user_id,vehicle_id,transfer_date,amount) values (1, 2, 1, current_date, '12.000');